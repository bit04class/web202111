-- 단일행 함수
-- 1.문자함수
--구 분 설 명
--LOWER 소문자로 변환한다.
--UPPER 대문자로 변환한다.
--INITCAP 첫 글자만 대문자로 나머지 글자는 소문자로 변환한다.
--CONCAT 문자의 값을 연결한다.
--SUBSTR 문자를 잘라 추출한다. (한글 1Byte)
--SUBSTRB 문자를 잘라 추출한다. (한글 2Byte)
--LENGTH 문자의 길이를 반환한다.(한글 1Byte)
--LENGTHB 문자의 길이를 반환한다.(한글 2Byte
--INSTR 특정 문자의 위치 값을 반환한다.(한글 1Byte)
--INSTRB 특정 문자의 위치 값을 반환한다.(한글 2Byte)
--LPAD, RPAD 입력 받은 문자열과 기호를 정렬하여 특정 길이의 문자열로 반환한다.
--TRIM 잘라내고 남은 문자를 표시한다.
--CONVERT CHAR SET을 변환한다.
--CHR ASCII 코드 값으로 변환한다.
--ASCII ASCII 코드 값을 문자로 변환한다.
--REPLACE 문자열에서 특정 문자를 변경한다

select 'aAbBcCdD',lower('aAbBcCdD'),upper('aAbBcCdD') from dual;
select ename, empno, sal from emp where lower(ename)='scott';

select initcap('aAbB cCdD') from dual;

select concat('문자','열') from dual;
select concat('문',concat('자','열')) from dual;  -- 문+자+열

select substr('aAbBcCdD',1,3) from dual;
select substrb('aAbBcCdD',1,3) from dual;

select substr('한글로작성',1,2) from dual;
select substrb('한글로작성',1,6) from dual;

select LENGTH('한글로작성') from dual;
select LENGTHb('한글로작성') from dual;

select instr('한글로작성','로') from dual;
select instrb('한글로작성','로') from dual;

select replace('한글로작성','로','만') from dual;

select lpad('abc',10,'#') from dual;
select rpad('abc',10,'#') from dual;

select lpad(rpad(trim('   ab c  '),20,'#'),30,'#') from dual;

select trim('   ab cd   ') from dual;
select trim('#' from '####ab#cd####') from dual;


-- 숫자함수
--ABS 절대값을 반환한다.
--COS COSINE 값을 반환한다.
--EXP e (2.71828183…)의 n승을 반환한다.
--FLOOR 소수점 아래를 잘라낸다.(버림)
--LOG LOG값을 반환한다.
--POWER POWER(m, n) m의 n승을 반환한다
--SIGN SIGN (n) n<0이면 –1, n=0이면 0, n>0이면 1을 반환한다.
--SIN SINE값을 반환한다.
--TAN TANGENT값을 반환한다.
--ROUND 특정 자릿수에서 반올림한다.
--TRUNC 특정 자릿수에서 잘라낸다. (버림)
--MOD 입력 받은 수를 나눈 나머지 값을 반환한다.

select 5/2,floor(5/2),mod(5,2) from dual;
select round(3.14),round(3.54) from dual;
select trunc(123.456) from dual;
select trunc(123.456,-1) from dual;
select trunc(123.456,-2) from dual;
select trunc(123.456,1) from dual;
select trunc(123.456,2) from dual;

-- 날짜함수
--구 분			설 명 
--SYSDATE		시스템 저장된 현재 날짜를 반환한다.
--MONTHS_BETWEEN 두 날짜 사이가 몇 개월인지를 반환한다.
--ADD_MONTHS	특정 날짜에 개월 수를 더한다.
--NEXT_DAY		특정 날짜에서 최초로 도래하는 인자로 받은 요일의 날짜를 반환한다.
--LAST_DAY		해당 달의 마지막 날짜를 반환한다.
--ROUND			인자로 받은 날짜를 특정 기준으로 반올림한다.
--TRUNC			인자로 받은 날짜를 특정 기준으로 버림한다
select sysdate from dual;
select hiredate,sysdate,sysdate-hiredate from emp;

select MONTHS_BETWEEN(sysdate,hiredate) from emp;

select sysdate-1 from dual;
select ADD_MONTHS(sysdate,-12) from dual;

select NEXT_DAY(sysdate,'월요일') from dual;
select LAST_DAY(sysdate+20) from dual;

select round(sysdate,'mm') from dual;
select trunc(sysdate,'mm') from dual;

select length(to_char(1234)) from dual;
select to_char(1234,'L999,999.99') from dual;
select to_char(sysdate,'YY-MM-DD HH:MI:SS') from dual;

select to_date('220120 1200','YYMMDD HHMI')+1 from dual;

select to_number('12.34')+1 from dual;

select null+1234 from dual;

select ename, sal, nvl(comm,0), sal+nvl(comm,0) from emp;

select ename,decode(deptno,10,'관리팀',20,'영업팀',30,'총무팀') from emp;

select ename, 
			case 
				when deptno=10 then '관리팀'
				when deptno=20 then '영업팀'
				when deptno=30 then '총무팀'
				else '몰라'
			end 
		from emp;

