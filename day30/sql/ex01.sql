-- DML (select,insert,update,delete) - CRUD
select * from dept;

-- 입력
-- insert into 테이블명 (컬럼명,...) values (값,...);
insert into dept (deptno,dname,loc) values (50,'aaaa','한글');

-- 삭제
-- delete from 테이블명 where 조건;
delete from dept where deptno=50 and dname='aaaa';

-- 수정
-- update 테이블명 set 컬럼명=값,... where 조건;
update dept set dname='AA',loc='BB' where deptno=50;
-------------------------------

insert into dept values (80,'FF','');
delete from dept where deptno in (50,60);
delete from dept where deptno between 50 and 60;
delete from dept where dname like '__';
 





