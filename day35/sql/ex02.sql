--old join	(2,4,5)
--	1.Cross Join
--	2.Equi Join
--	3.NonEqui Join
--	4.Outer Join
--	5.Self Join
--
--ANSI 조인	(2,4)
--	1.select * from ex01 cross join ex02
--	2.select * from ex01 inner join ex02 on ex01.num=ex02.num
--	2-1.select * from ex01 inner join ex02 using (num) -> 중복된 컬럼명이 하나이상 필수
--	2-3.select * from ex01 natural join ex02 -> 반드시 하나의 컬럼명이 중복되어야함
--  4-1.select * from ex01 left outer join ex02 on ex01.num=ex02.num
--  4-2.select * from ex01 right outer join ex02 on ex01.num=ex02.num
--  4-3.select * from ex01 full outer join ex02 on ex01.num=ex02.num
--  
drop table ex01;
drop table ex02;
create table ex01(
	idx number,
	name varchar2(10),
	num number
);
create table ex02(
	num number,
	loc	varchar2(6)
);
insert into ex01 values (1,'user1',1111);
insert into ex01 values (2,'user2',2222);
insert into ex01 values (3,'user3',3333);
insert into ex01 values (4,'user4',5555);

insert into ex02 values (1111,'서울');
insert into ex02 values (2222,'부산');
insert into ex02 values (3333,'대전');
insert into ex02 values (4444,'제주');
commit;

















