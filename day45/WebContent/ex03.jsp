<?xml version="1.0" encoding="UTF-8"?>
<%@ page language="java" contentType="application/xml; charset=UTF-8"
    pageEncoding="UTF-8"%>
<students>
<%
int cnt=Integer.parseInt(request.getParameter("cnt"));
for(int i=0; i<cnt; i++){
%>
	<student>
		<num><%=i %></num>
		<name>user<%=i %></name>
		<kor><%=i+90 %></kor>
		<eng><%=i+80 %></eng>
		<math><%=i+70 %></math>
	</student>
<%
}
%>
</students>