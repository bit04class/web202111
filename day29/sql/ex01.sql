-- 주석
-- 테이블 목록 확인
select tname from tab;
-- 사원정보테이블(emp)
select * from emp;
-- 부서정보테이블(dept)
select * from dept;
-- dbms
-- 관계형 데이터베이스 모델 (RDBMS)
-- 순서가 없는 집합

-- 테이블:세로줄(컬럼)과 가로줄(row,레코드)의 모델을 이용하여 정렬된 데이터 집합(값)의 모임
-- sql 종류
-- DML - 데이터 조작 (select - dql)
-- DDL - 데이터 설계&정의
-- TCL - 트렌젝션 제어
-- DCL - 데이터베이스 컨트롤


select * from dept;
select * from emp;

select deptno,dname from dept;
-- 사원의 이름과 급여와 입사일자만을 출력하는 SQL 문을 작성해보자
SELECT ENAME,sal,hiredate from EMP;

select distinct job,deptno from emp;

select distinct empno,job from emp;
select deptno from dept;
select deptno from emp;
select distinct deptno from emp;
select ename,sal from emp where sal>=3000;
select * from emp where deptno=10;
select empno,ename,sal from emp where sal<2000;
select tname from tab;
select * from emp;
select empno,ename,sal from emp where ename='scott';
select empno,ename,sal from emp where ename='SCOTT';
select empno,ename,job from emp where ename='MILLER';
select ename,hiredate from emp where hiredate>='1985/01/01';
select ename,hiredate from emp where hiredate<='1985/01/01';
select ename,hiredate from emp where hiredate<'1981/01/01';
select ename,hiredate from emp where hiredate<'81/01/01';
select ename,hiredate from emp where hiredate<'19810101';
select ename,hiredate from emp where hiredate>='1981/01/01' and hiredate<='1981/12/31';
-- 급여가 1000에서 3000 사이에 있는 사원
select ename,sal from emp where sal>=1000 and sal<=3000;
select ename,sal from emp where sal between 1000 and 3000;
-- 사원번호가 7844이거나 7654이거나 7521인 사원
select empno,ename from emp where empno=7844 or empno=7654 or empno=7521;
select empno,ename from emp where empno in (7844,7654,7521);

-- 급여가 1500과 2500 사이인 사원의 사번, 이름, 급여를 출력하라.
select empno,ename,sal from emp where sal between 1500 and 2500;






